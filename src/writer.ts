export class Writer {
    color1 = 0xff;
    color2 = 0x00;

    GenerateTexture = (buffer: Buffer, size: any): void => {
        for (var y = 0; y < size.height; y++) {
            for (var x = 0; x < size.width; x++) {
                let idx = (size.width * y + x) << 2;
 
                const color = x%2 == 0 ? this.color1: this.color2;

                buffer[idx] = color;
                buffer[idx + 1] = color;
                buffer[idx + 2] = color;
                buffer[idx + 3] = 0xff;
            }
        }
    }
}