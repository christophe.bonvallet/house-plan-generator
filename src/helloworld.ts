import fs from 'fs';
import { PNG }  from 'pngjs'
import { Writer } from './writer';

const size = {
  width:900,
  height: 600
}
// Get document, or throw exception on error
let newfile = new PNG({width:size.width,height:size.height});
let writer = new Writer();
writer.GenerateTexture(newfile.data, size);

newfile.pack()
  .pipe(fs.createWriteStream(__dirname + '/newfile.png'))
  .on('finish', function() {
    console.log('Written!');
  });
